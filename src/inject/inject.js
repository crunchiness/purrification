var POST_NOTIFICATION = '<li class="notification single_notification" purr=true style="display: list-item">\n'
                            + '  <div class="notification_inner clearfix">\n'
                            + '    <div class="notification_sentence">\n'
                            + '      <div class="hide_overflow">\n'
                            + '        A <a class="notification_target" href="{{url}}" '
                            + 'target="_blank">post</a> by <a class="{{username}}" '
                            + 'href="http://{{username}}.tumblr.com">{{username}}</a></div>\n'
                            + '      </div>\n'
                            + '    </div>\n'
                            + '  <a target="_blank" href="http://{{url}}" class="avatar_frame">\n'
                            + '    <img alt="{{username}}" class="avatar" id="notification_avatar_purrified"'
                            + ' src="http://api.tumblr.com/v2/blog/{{username}}.tumblr.com/avatar/40" '
                            + 'data-src="http://api.tumblr.com/v2/blog/{{username}}.tumblr.com/avatar/40">\n'
                            + '  </a>\n'
                            + '</li>\n';
var purr = {
    current_dash2: false,
    settings: {},
    form_key: 'no key',
    myLocation: function () {
        if (purr.current_dash2)
            return 1;
        if (tinyFunctions.checkurl(window.location.href, ['http://www.tumblr.com/dashboard*']))
            return 0;
        if (tinyFunctions.checkurl(window.location.href, ['http://www.tumblr.com/tagged/*',
            'http://www.tumblr.com/likes*',
            'http://www.tumblr.com/blog/*',
            'http://www.tumblr.com/liked/by/*'
        ]))
            return 2;
        return 3;
    },
    dash2_init : function (settings) {
        var submenu = '<div id="purr_submenu" style="display:none"><ul>';
        for (var dash in settings.dashboards)
            submenu += '<li><a href="#" id="purr_' + settings.dashboards[dash].id + '">'
                    + settings.dashboards[dash].dash_name
                    + '</a></li>';
        submenu += '</ul><span class="tab_notice_nipple"></span></div>';

        var link = '<div class="tab iconic " id="dash2_button">'
                 + '<a id="cool_icon" href="#">Dash 2</a>'
                 + submenu
                 + '<div class="selection_nipple"></div></div>';

        $(link + submenu).insertAfter('#home_button');
        $('#dash2_button').click( function () { $('#purr_submenu').toggle(); });

        for (var dash in settings.dashboards) {
            var dash_id = settings.dashboards[dash].id;
            $('#purr_' + dash_id).click({dash_id : dash_id, settings: settings}, function(event) {
                purr.dash2_open(event.data.dash_id, event.data.settings);
            });
        }
    },
    dash2_open : function (dash_id, settings) {
        if (purr.current_dash2) {
            $('ol#posts > li:not(.new_post)').remove();
            angular.element(document.getElementById('posts')).scope().$apply(function(scope) {
                scope.getDash(dash_id);
            });
        } else {
            $('ol#posts').attr('ng-app');
            $('ol#posts').attr('ng-controller', 'Dash2Ctrl');
            $('ol#posts').attr('ng-init', 'getDash(' + dash_id + '); getPosts();');
            $('ol#posts').attr('infinite-scroll', 'getPosts()');
            $('ol#posts').attr('infinite-scroll-distance', '4');
            $('div#dash2_button > div.selection_nipple').css('display','block');
            $('div#dash2_button').addClass('selected');
            $('div#home_button > div.selection_nipple').css('display','none');
            $('div#home_button').removeClass('selected');
            $('ol#posts > li:not(.new_post)').remove();
//TODO new disabler
            if ($('#loading_text').length == 0) {
                var loading_text = '<div id="loading_text" style="display: none">Loading... ' +
                    '<span id="loading_purr"></span></div>';
                $('div#left_column').append(loading_text);
            } else {
                $('#loading_text').html('Loading... <span id="loading_purr"></span>');
            }
            $('div.spinner').css('background-image',
                    'url("' + chrome.extension.getURL('imgs/auto_pagination_loader_purr.gif') + '")');
            angular.bootstrap($('ol#posts')[0], ['dash2app']);
        }
        purr.current_dash2 = true;
    },
    addVideoLink : function (post) {
        if ($(post).find('div.tumblr_video_container').length > 0) {
            var id = (post.id).split('_')[1];
            var username = $(post).find('div.post_info > a')[0].text;
            var api_url = 'http://api.tumblr.com/v2/blog/' + username
                        + '.tumblr.com/posts?api_key='
                        + api_key
                        + '&id='
                        + id;
            var xhr = new XMLHttpRequest();
            xhr.open('GET', api_url, true);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4) {
                    var downloadLink = '<div class="caption" style="margin-top: 0px; background: #eee">'
                        + '<a href="' + JSON.parse(xhr.responseText).response.posts[0].video_url + '"'
                        + ' style="font-size: 10px" target="_blank">Download</a></div>';
                    $(downloadLink).insertAfter($('div#tumblr_video_container_' + id));
                }
            
            };
            xhr.send();
        }
    }
};

$(document).on('DOMNodeInserted', function(e) {
    if (e.target.tagName == 'LI' && (e.target.className.indexOf("post_container") !== -1 || e.target.className.indexOf("notification") !== -1)) {
        if (!purr.settings.nofilter) {
            switch(purr.myLocation()) {
                case 0: // main dashboard
                    doFiltering(e.target);break;
                case 1: // secondary dashboard
                    if (e.target.className.indexOf("dash2") === -1)
                        e.target.remove();break;
                case 2: // other location with posts
                    $(e.target).css('display', 'list-item');
                    purr.addVideoLink(e.target);break;
                default:
                    $(e.target).css('display', 'list-item');
            }
        } else {
            $(e.target).css('display', 'list-item');
        }
    }
});

chrome.extension.sendMessage({get : 'settings'}, function(response) {
    purr.settings = response;
    purr.form_key = $('#tumblr_form_key')[0].content;
        if ($('div#user_tools').length > 0)
            purr.dash2_init(purr.settings);
        if (!purr.settings.nofilter) {
            $('li').each(function(i, e) {
                if (e.tagName == 'LI' && (e.className.indexOf("post_container") !== -1 || e.className.indexOf("notification") !== -1)) {
                    switch(purr.myLocation()) {
                        case 0: // main dashboard
                            doFiltering(e);break;
                        case 1: // secondary dashboard
                            if (e.className.indexOf("dash2") === -1)
                                e.remove();break;
                        case 2: // other location with posts
                            $(e).css('display', 'list-item');
                            purr.addVideoLink(e);break;
                        default:
                            $(e).css('display', 'list-item');
                    }
                } else {
                    if (e.className.indexOf("post_container") !== -1) {
                        $(e).css('display', 'list-item');
                    }
                }
            });
        } else {
            $('li.post_container').css('display', 'list-item');
            $('li.post_container').css('display', 'list-item');
        }
});

function doFiltering(post) {
    if (post === undefined) return;
    if (post.tagName != 'LI') return;
    if (post.className.split(' ')[0] == 'notification') {
        if (!$(post).attr('purr')) {
            if (purr.settings.hide_notifs)
                $(post).remove();
            else
                $(post).css('display', 'list-item');
            return;
        }
    }
    if (post.className.split(' ')[0] != 'post_container') return;
    if (post.id == 'new_post_buttons') {
        $(post).css('display', 'list-item');
        return;
    }
    for (var i in purr.settings.users) {
        if (purr.settings.users[i].blocked) {
            var username = purr.settings.users[i].username;
            var post_author = $(post).find('div.post').attr('data-tumblelog-name');
            if (username == post_author) {
                if (purr.settings.show_purr_notifs) {
                    var view = {
                            url : $(post).find('a.permalink').attr('href'),
                            username : username
                    };
                    $(Mustache.render(POST_NOTIFICATION, view)).insertAfter(post);
                }
                return;
            }
        }
    }
    $(post).css('display', 'list-item');
    purr.addVideoLink(post);
}
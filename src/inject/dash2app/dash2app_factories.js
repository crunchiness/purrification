var dash2app = angular.module('dash2app', ['ngResource', 'infinite-scroll']).config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');});

dash2app.factory('PostR', ['$resource',
    function ($resource) {
        return $resource('http://api.tumblr.com/v2/blog/:user.tumblr.com/posts/:type', {
            user: '@user',
            type: '@type'
        }, {
            get: {
                method: 'GET',
                params: {
                    reblog_info: true,
                    api_key: '@api_key'
                },
                isArray: false
            }
        });
// api_key : "4EZDirQaxZqK8G3o32VBPkpfU9tMqdYvtBEEjKrSnPSI44BEka"
// limit : 10,
// offset : 2,
    }
]);

dash2app.factory('LoadPosts', ['PostR','$q',
    function (PostR, $q) {
        return {
            load: function (user, type, api_key, limit, offset) {
                var delay = $q.defer();
                PostR.get({
                        user: user,
                        type: type,
                        api_key: api_key,
                        limit: limit,
                        offset: offset
                    },
                    function (posts) {
                        delay.resolve(posts);
                    },
                    function (reason) {
                        var error = {
                                user: user,
                                error: reason
                        };
                        delay.reject(error);
                    });
                return delay.promise;
            }
        };
    }
]);
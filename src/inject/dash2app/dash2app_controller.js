function Dash2Ctrl($scope, $q, LoadPosts) {
    $scope.loadingNow = false;
    $scope.posts = [];
    $scope.posts_on_display = [];
    $scope.dash = {};
    $scope.getDash = function (dash_id) {
        $scope.loadingNow = false;
        delete $scope.dash;
        $scope.posts = [];
        $scope.posts_on_display = [];
        var users = purr.settings.users;
        var dashboards = purr.settings.dashboards;
        var dash_name = '';
        for (var i in dashboards) {
            if (dashboards[i].id == dash_id) {
                dash_name = dashboards[i].dash_name;
            }
        }
        var contents = [];
        for (var i in users) {
            if (!users[i].not_found) {
                var dashes = users[i].dashes;
                for (var j in dashes) {
                    if (dashes[j].dash == dash_id) {
                        var types = dashes[j].types;
                        if (types[0]) {
                            var user_t = {
                                    username: users[i].username,
                                    offset: 0,
                                    limit: 10,
                                    limitoffset: 0
                            };
                            contents.push(user_t);
                        } else {
                            for (var k = 1; k < 9; k++) {
                                if (types[k]) {
                                    var user_t = {
                                            username: users[i].username,
                                            type: k,
                                            offset: 0,
                                            limit: 10,
                                            limitoffset: 0
                                    };
                                    contents.push(user_t);
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.dash = {
            dash_id : dash_id,
            dash_name : dash_name,
            contents : contents
        };
    };
    $scope.getPosts = function () {
        if ($scope.loadingNow) return;
        $scope.loadingNow = true;
        users = $scope.dash.contents;
        var promises = [];
    	for (var i in users) {
	    	if (users[i].limit !== 0) {
	    		var type = '';
	    		switch(users[i].type) {
	    			case 1:
	    				type = 'text';break;
	    			case 2:
	    				type = 'photo';break;
	    			case 3:
	    				type = 'quote';break;
	    			case 4:
	    				type = 'link';break;
	    			case 5:
	    				type = 'chat';break;
	    			case 6:
	    				type = 'audio';break;
	    			case 7:
	    				type = 'video';break;
	    			case 8:
	    				type = 'answer';break;
	    		}
			
                var promise = LoadPosts.load(users[i].username, type, purr.settings.api_key, users[i].limit, users[i].offset);
                promises.push(promise);
	    	}
	    }

        $q.all(promises)
        .then(function(raw_posts) {
            for (var i in raw_posts) {
                $scope.posts = $scope.posts.concat(raw_posts[i].response.posts);
            }
            if ($scope.posts.length == 0) {
                $('#loading_text').html('End of the dash#2');
                $('#loading_text').css('display', 'block');
                $('div.spinner').remove();
                $('#auto_pagination_loader').remove();
            }
            $scope.posts.sort(tinyFunctions.compare);
// add an array of last ids???            
            var posts_to_add = $scope.posts.slice(0,10);
            $scope.posts = $scope.posts.slice(10);

            for (var i in $scope.dash.contents) {
                $scope.dash.contents[i].offset = $scope.dash.contents[i].offset + $scope.dash.contents[i].limit;
                var limit = 0;
                for (var j in posts_to_add) {
                    if (posts_to_add[j].blog_name === $scope.dash.contents[i].username) {
                        limit++;
                    }
                }
                $scope.dash.contents[i].limit = limit + $scope.dash.contents[i].limitoffset;
                $scope.dash.contents[i].limitoffset = 0;
			}
            for (i in posts_to_add) {
                var post = new Post(posts_to_add[i], purr.form_key);
                $('ol#posts').append(post.returnHtml());
                $scope.posts_on_display.push(post.id);
            }
            $scope.loadingNow = false;
            $('div#dashboard_controls').css('display', 'none');
            $('#loading_purr').html('');
/*
            if (???) {
                $('#loading_text').html('End of the dash#2');
                $('div.spinner').remove();
                $('#auto_pagination_loader').remove();
			}
*/
        }, function(reason) {
            console.log(reason);
            if (reason.error.status == 404) {
                for (var i in purr.settings.users) {
                    if (purr.settings.users[i].username == reason.user) {
                        purr.settings.users[i].not_found = true;
                        chrome.extension.sendMessage({action: 'not_found', user: reason.user});
                        alert('User ' + reason.user + ' not found!');
                        purr.dash2_open($scope.dash.dash_id, purr.settings);
                        break;
                    }
                }
            }
            return $q.reject(reason);
        });
    };
}

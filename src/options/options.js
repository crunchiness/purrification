var optionsApp = angular.module('optionsApp', []);
optionsApp.filter('shortenToChars',function() {
    var shorten = function(input, maxLength) {
        return (input.length > maxLength) ? input.substr(0, maxLength) + '...' : input;
    };
    return shorten;
});

optionsApp.filter('indash', function() {
    return function(users, dash_id) {
        var filtered_users = [];
        for (var i in users) {
            for (var j in users[i].dashes) {
                if (users[i].dashes[j].dash === dash_id) {
                    filtered_users.push(users[i]);
                }
            }
        }
        return filtered_users;
    };
});

optionsApp.filter('typeToString', function() {
    return function(type) {
        switch(type) {
            case 0:
                return 'all';
            case 1:
                return 'text';
            case 2:
                return 'photo';
            case 3:
                return 'quote';
            case 4:
                return 'link';
            case 5:
                return 'chat';
            case 6:
                return 'audio';
            case 7:
                return 'video';
            case 8:
                return 'answer';
        }
    };
});

optionsApp.filter('notFoundClass', function() {
    return function(not_found) {
        if (not_found) {
            return 'not_found';
        } else {
            return '';
        } 
    };
});

optionsApp.directive('confirmClick', function () {
    return {
        link: function (scope, element, attr) {
            var thing = scope.$eval(attr.confirmClick);
            var msg = "Are you sure?";
            if (thing && attr.title == "remove user") {
                msg = "Do you really want to remove " + thing + "? (removes from all dashboards)";
            } else if (thing) {
                msg = "Do you really want to remove " + thing + "?";
            }
            var clickAction = attr.confirmedClick;
            element.bind('click', function (event) {
                if (window.confirm(msg)) {
                    scope.$eval(clickAction);
                }
            });
        }
    };
});

function OptionsCtrl($scope) {
    function load() {
        chrome.storage.local.get(null, function(response) {
            $scope.$apply(function() { $scope.loadOptions(response); });
        });
    }
    function save($scope) {
        var settings = {
                version : $scope.version,
                api_key : $scope.api_key,
                hide_notifs : $scope.hide_notifs,
                show_purr_notifs : $scope.show_purr_notifs,
                users : $scope.users,
                dashboards : $scope.dashboards,
                max_dash_id : $scope.max_dash_id,
                nofilter: $scope.nofilter
        };
        chrome.storage.local.set(settings, function() {
            if (chrome.runtime.lastError !== undefined) {
                var error = chrome.runtime.lastError.message;
                console.log(error);
                load();
            }
        });
        chrome.extension.sendMessage({action: 'update'});
    }
    $scope.$watch('hide_notifs', function(newValue, oldValue) {
        save($scope);
    }, true);
    $scope.$watch('show_purr_notifs', function(newValue, oldValue) {
        save($scope);
    }, true);
    $scope.$watch('dashboards', function(newValue, oldValue) {
        //do thing where remove dash from users
        save($scope);
    }, true);
    $scope.$watch('users', function(newValue, oldValue) {
        save($scope);
    }, true);
    $scope.$watch('max_dash_id', function(newValue, oldValue) {
        save($scope);
    }, true);
    $scope.$watch('nofilter', function(newValue, oldValue) {
        save($scope);
    }, true);
    load();
    
    $scope.removeType = function (username, dash_id, type) {
        for (var i in $scope.users) {
            if ($scope.users[i].username == username) {
                for (var j in $scope.users[i].dashes) {
                    if ($scope.users[i].dashes[j].dash == dash_id) {
                        var index = $scope.users[i].dashes[j].types.indexOf(type);
                        if (index > -1) {
                            $scope.users[i].dashes[j].types.splice(index, 1);
                            return;
                        }
                    }
                }
            }
        }
    };
    
    $scope.removeUser = function (username) {
        $scope.$apply(function () {
            for (var i = 0; i < $scope.users.length; i++) {
                if ($scope.users[i].username == username) {
                    $scope.users.splice(i,1);
                    return;
                }
            }
        });
    };
    
    $scope.removeDash = function (dash_id) {
        $scope.$apply(function () {
            for (var i in $scope.users) {
                for (var j = 0; j < $scope.users[i].dashes.length; j++) {
                    if ($scope.users[i].dashes[j].dash == dash_id) {
                        $scope.users[i].dashes.splice(j,1);
                        break;
                    }
                }
            }
            for (var i = 0; i < $scope.dashboards.length; i++) {
                if ($scope.dashboards[i].id == dash_id) {
                    $scope.dashboards.splice(i,1);
                    return;
                }
            }
        });
    };
    
    $scope.addUser = function () {
        var newUser = {
                username: $scope.newUser,
                blocked: true,
                not_found: false,
                dashes: []
        };
        $scope.users.push(newUser);
        $scope.newUser = '';
        $scope.newUserBlocked = false;
    };
    $scope.addDashboard = function () {
        $scope.max_dash_id++;
        var newDashboard = {
                id: $scope.max_dash_id,
                dash_name: $scope.newDashName
        };
        $scope.dashboards.push(newDashboard);
        $scope.newDashName = '';
    };
    $scope.addUserToDash = function (dash_id, newUser) {
        if (newUser === undefined || newUser == '') return;
        for (var i in $scope.users) {
            if ($scope.users[i].username == newUser) {
                var dash = {
                        dash : dash_id,
                        types : [true, false, false, false, false, false, false, false, false]
                };
                $scope.users[i].dashes.push(dash);
                return;
            }
        }
        var user = {
                username: newUser,
                blocked: false,
                not_found: false,
                dashes: [{dash: dash_id, types : [true, false, false, false, false, false, false, false, false]}]
        };
        $scope.users.push(user);
    };
    $scope.removeUserFromDash = function (username, dash_id) {
        for (var i = 0; i < $scope.users.length; i++) {
            if ($scope.users[i].username == username) {
                for (var j = 0; j < $scope.users[i].dashes.length; j++) {
                    if ($scope.users[i].dashes[j].dash == dash_id) {
                        $scope.users[i].dashes.splice(j,1);
                        return;
                    }
                }
            }
        }
    };
    $scope.loadOptions = function (response) {
        console.log(response);
        if (response) {
            $scope.version = response.version;
            $scope.api_key = response.api_key;
            $scope.hide_notifs = response.hide_notifs;
            $scope.show_purr_notifs = response.show_purr_notifs;
            $scope.users = response.users;
            $scope.dashboards = response.dashboards;
            $scope.max_dash_id = response.max_dash_id;
            $scope.nofilter = response.nofilter;
        }
    };
}
//example of using a message handler from the inject scripts
//TODO on install save default/convert
var purrification_version = '2.0.0';
var api_key = '4EZDirQaxZqK8G3o32VBPkpfU9tMqdYvtBEEjKrSnPSI44BEka';

var default_settings = {
    'version': purrification_version,
    'api_key': api_key,
    'hide_notifs': true,
    'show_purr_notifs': true,
    'nofilter': false,
    'users': [{
            'username': 'thetestblog17',
            'blocked': false,
            'not_found': false,
            'dashes': [{
                'dash': 0,
                'types': [false, true, true, false, false, false, false, false, false]
            }, {
                'dash': 2,
                'types': [false, false, false, false, false, true, false, false, false]
            }]
        }, {
            'username': 'staff',
            'blocked': true,
            'not_found': false,
            'dashes': [{
                'dash': 0,
                'types': [false, false, true, true, false, false, true, false, false]
            }, {
                'dash': 2,
                'types': [false, false, false, false, true, false, false, false, false]
            }]
        }, {
            'username': 'crunchiness',
            'blocked': false,
            'not_found': false,
            'dashes': [{
                'dash': 1,
                'types': [false, true, true, true, false, false, true, false, false]
            }, {
                'dash': 2,
                'types': [false, false, false, false, true, false, false, false, false]
            }]
        }, {
            'username': 'purrification',
            'blocked': true,
            'not_found': false,
            'dashes': [{
                'dash': 1,
                'types': [false, false, false, true, false, false, true, false, false]
            }, {
                'dash': 2,
                'types': [false, true, true, false, false, true, false, false, false]
            }]
        }],
    'dashboards': [{
            'id': 0,
            'dash_name': 'dash 1'
        }, {
            'id': 1,
            'dash_name': 'dash 2'
        }, {
            'id': 2,
            'dash_name': 'dash 3'
        }],
    'max_dash_id': 2
};

var settings = {};
loadSettings();
function loadSettings() {
    chrome.storage.local.get(null, function(response) {
        if (tinyFunctions.is_empty(response)) {
            settings = default_settings;
        } else {
            settings = response;
        }
    });
}

chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
    chrome.pageAction.show(sender.tab.id);
    if (request.get == 'settings') {
        sendResponse(settings);
    }
    if (request.action == 'update') {
        loadSettings();
    }
    if (request.action == 'not_found') {
        for (var i in settings.users) {
            if (settings.users[i].username == request.user) {
                settings.users[i].not_found = true;
                chrome.storage.local.set(settings);
                break;
            }
        }
    }
});
function typeTextToBinary(text) {
    var types_binary = [false, false, false, false, false, false, false, false, false];
    switch(text) {
        case 'all':
            types_binary[0] = true;
            break;
        case 'text':
            types_binary[1] = true;
            break;
        case 'photo':
            types_binary[2] = true;
            break;
        case 'quote':
            types_binary[3] = true;
            break;
        case 'link':
            types_binary[4] = true;
            break;
        case 'chat':
            types_binary[5] = true;
            break;
        case 'audio':
            types_binary[6] = true;
            break;
        case 'video':
            types_binary[7] = true;
            break;
        case 'answer':
            types_binary[8] = true;
            break;
    }
    return types_binary;
}

function updateSettings() { 
    chrome.storage.sync.get(null, function (settings) {
        if (!tinyFunctions.is_empty(settings)) {
            if (settings['version'] == '1.3.6.1') {
                var new_settings;
                var nofilter = false;
                var max_dash_id = 0;
                var hide_notifs = settings['regular_notif'];
                var show_purr_notifs = !settings['purr_notif'];
                var types_binary = [true, false, false, false, false, false, false, false, false];
                var users = [];
                var dashboards = [{
                    'id': 0,
                    'dash_name': 'dash 1'
                }];
                
                for (var i in settings['users']) {
                    var dashes = [];
                    if (settings['users'][i].snd_dash) {
                        dashes = [{ dash: 0, types: types_binary }];
                    }
                    var user = {
                            username: settings['users'][i].user,
                            blocked: settings['users'][i].blocked,
                            not_found: false,
                            dashes: dashes
                    };
                    users.push(user);
                }
                new_settings = {
                        version: purrification_version,
                        api_key: api_key,
                        hide_notifs: hide_notifs,
                        show_purr_notifs: show_purr_notifs,
                        users: users,
                        dashboards: dashboards,
                        max_dash_id: max_dash_id,
                        nofilter: nofilter
                };
                chrome.storage.local.set(new_settings, function() {
                    loadSettings();
                });
            } else {
                chrome.storage.local.set(default_settings, function() {
                    loadSettings();
                });
            }
        } else {
            chrome.storage.local.set(default_settings, function() {
                loadSettings();
            });
        }
    });
}

chrome.runtime.onInstalled.addListener(function(details) {
    updateSettings();
});
var POST_TEMPLATE = '<li class="post_container dash2" style="display: list-item"><div class="post post_full {{post_type.class_name}} not_mine is_reblog with_permalink" id="post_{{post_id}}" data-post-id="{{post_id}}" data-root-id="{{root_id}}" data-tumblelog-name="{{tumblelog_name}}" data-tumblelog-key="{{tumblelog_key}}" data-reblog-key="{{reblog_key}}" data-type="{{post_type.data_type}}" data-following-tumblelog="1" data-view-exists="true"><div class="post_avatar show_user_menu"><a class="post_avatar_link" href="http://{{tumblelog_name}}.tumblr.com/" target="_blank" id="post_avatar_{{post_id}}" style="background-image:url(\'http://api.tumblr.com/v2/blog/{{tumblelog_name}}.tumblr.com/avatar/64\')" data-user-avatar-url="http://api.tumblr.com/v2/blog/{{tumblelog_name}}.tumblr.com/avatar/64" data-avatar-url="http://api.tumblr.com/v2/blog/{{tumblelog_name}}.tumblr.com/avatar/64" data-blog-url="http://{{tumblelog_name}}.tumblr.com/" data-use-channel-avatar="1" data-use-sub-avatar=""><img class="post_avatar_image" src="http://api.tumblr.com/v2/blog/{{tumblelog_name}}.tumblr.com/avatar/64" width="64" height="64"></a><div id="tumblelog_menu" class="tumblelog_menu post_avatar_user_menu"><div class="tumblelog_menu_button"><i>Tumblelog Menu</i></div><div id="popover_tumblelog_menu" class="popover popover_menu popover_gradient tumblelog_menu_popover user_menu" style="display:none;"><div class="popover_inner"><div class="popover_menu_item"><a data-tumblelog-name="{{tumblelog_name}}" class="tumblelog_menu_link ask" href="http://{{tumblelog_name}}.tumblr.com/ask" target="_new" title="Ask"><span class="hide_overflow">Ask</span></a></div><div class="popover_menu_item"><a class="tumblelog_menu_link fan_mail" href="http://www.tumblr.com/send/{{tumblelog_name}}?redirect_to=http%3A%2F%2Fwww.tumblr.com" target="_new" title="Fan Mail"><span class="hide_overflow">Fan Mail</span></a></div><!--<div class="popover_menu_item"><a class="user_menu_toggle_follow tumblelog_menu_link unfollow"><span class="hide_overflow"><span class="follow">Follow</span><span class="unfollow">Unfollow</span></span></a></div>--><div class="popover_menu_item"><a class="tumblelog_menu_link open_in_tab" href="http://{{tumblelog_name}}.tumblr.com" target="_{{tumblelog_name}}" title="Open in Tab"><span class="hide_overflow">Open in Tab</span></a></div></div></div></div></div><div class="post_wrapper"><div class="post_header"><div class="post_info"><div class="post_info_fence{{#reblogged_name}} has_follow_button{{/reblogged_name}}"><a href="http://{{tumblelog_name}}.tumblr.com/">{{tumblelog_name}}</a>&nbsp;{{#reblogged_name}}<span class="reblog_source"><span class="reblog_icon" title="{{tumblelog_name}} reblogged {{reblogged_name}}">reblogged</span>&nbsp;<a title="{{reblogged_name}}" href="{{reblogged_url}}">{{reblogged_name}}</a></span>{{/reblogged_name}}</div>{{#reblogged_name}}<a href="/follow/{{reblogged_name}}" class="reblog_follow_button" data-tumblelog-name="{{reblogged_name}}" title="Follow {{reblogged_name}}"><i>Follow</i></a>{{/reblogged_name}}</div>{{#source_name}}<div class="post_source"><span class="post_source_name_prefix">Source: </span><a class="post_source_link" target="_blank" href="{{source_url}}" title="{{source_name}}">{{source_name}} </a></div>{{/source_name}}</div><div class="post_content clearfix"><div class="post_content_inner clearfix">{{#post_type.photo}}<div class="post_media"><img class="image_thumbnail" alt="" id="thumbnail_photo_{{post_id}}" data-full-size="{{image.url}}" data-thumbnail="{{image.url}}" data-width="{{image.width}}" data-height="{{image.height}}" data-thumbnail-width="{{image.alt_width}}" data-thumbnail-height="{{image.alt_height}}" style="cursor: pointer; background-color: transparent;" width="{{image.alt_width}}" height="{{image.alt_height}}" src="{{image.url}}"><div class="photo_info hidden"> <a class="high_res_link no_pop" href="http://{{tumblelog_name}}.tumblr.com/image/{{post_id}}" id="high_res_link_{{post_id}}">High-res</a> →</div></div>{{/post_type.photo}}{{#post_type.photoset}}<div class="post_media"><div id="photoset_{{post_id}}" class="photoset" style="margin-bottom: {{#caption}}11{{/caption}}{{^caption}}0{{/caption}}px;">{{#photoset}}<div class="photoset_row photoset_row_{{photos_in_row}}" style="height:{{minheight}}px; width:500px;">{{#row}}<a href="{{hdurl}}" class="photoset_photo" id="photoset_link_{{post_id}}_{{number}}"><img style="{{margin}}width:{{alt_width}}px;" src="{{url}}" alt=""></a>{{/row}}</div>{{/photoset}}</div></div>{{/post_type.photoset}}{{#post_type.audio}}<div class="post_media">{{{audio_embed}}}{{{audio_download}}}</div>{{/post_type.audio}}{{#post_type.link}}<div class="post_media"><div class="link_button clickable"><div class="link_text_container"><div class="link_text_outer"><div class="link_text">{{#post_title}}<a href="{{link_url}}" target="_blank" class="link_title">{{post_title}}</a><a href="{{link_url}}" target="_blank" class="link_source">{{link_url_text}}</a>{{/post_title}}{{^post_title}}<a href="{{link_url}}" target="_blank" class="link_title">{{link_url}}</a>{{/post_title}}</div></div></div></div></div>{{/post_type.link}}{{#post_type.answer}}<div class="post_body"><div class="clear"></div><div class="post_question "> {{question}}</div><div class="asking_avatar">{{^question_asker_url}}<img src="http://assets.tumblr.com/images/anonymous_avatar_24.gif" alt="" width="24" height="24"><span class="post_question_asker">Anonymous</span>{{/question_asker_url}}{{#question_asker_url}}<a href="{{question_asker_url}}"><img src="http://api.tumblr.com/v2/blog/{{question_asker}}.tumblr.com/avatar/24" alt="" width="24" height="24"></a><a href="{{question_asker_url}}" class="post_question_asker">{{question_asker}}</a>{{/question_asker_url}}</div><div style="margin-top: 15px; padding-top: 15px; border-top: 1px solid #e7eaec">{{{answer}}}</div></div>{{/post_type.answer}}{{#post_type.chat}}{{#post_title}}<div class="post_title">{{post_title}}</div>{{/post_title}}<div class="post_body"><ul class="conversation_lines">{{#chat_dialogue}}<li class="chat_line">{{{strong_label}}}{{phrase}}</li>{{/chat_dialogue}}</ul></div>{{/post_type.chat}}{{#post_type.video}}<div class="post_media">{{{video_player}}}{{#video_url}}<a href="{{video_url}}" style="font-size: 10px;" target="_blank">Download</a>{{/video_url}}</div>{{/post_type.video}}{{#caption}}<div class="post_body">{{{caption}}}</div>{{/caption}}{{#post_type.text}}{{#post_title}}<div class="post_title">{{post_title}}</div>{{/post_title}}<div class="post_body">{{{text_body}}}</div>{{/post_type.text}}{{#post_type.quote}}{{#quote_text}}<div class="post_title"> &ldquo;<span class="quote">{{quote_text}}</span>&rdquo;</div>{{/quote_text}}{{#quote_source}}<div class="post_body"><table class="quote_source_table"><tbody><tr><td valign="top" class="quote_source_mdash"> &mdash;&nbsp;</td><td valign="top" class="quote_source">{{{quote_source}}}</td></tr></tbody></table></div>{{/quote_source}}{{/post_type.quote}}</div></div><div class="post_tags"><div class="post_tags_inner">{{#post_type.answer}}<a clas="post_tag ask post_ask_me_link" href="http://{{tumblelog_name}}.tumblr.com/ask">Ask {{tumblelog_name}} a question</a>{{/post_type.answer}}{{#tags}}{{{full_tag}}}{{/tags}}</div></div><div class="post_footer clearfix"><div class="post_notes"><div class="post_notes_inner"><div class="post_notes_label"><!--<span class="note_link_less" title="7,691 notes">7,691 notes </span>--><span class="note_link_current" title="{{note_count}} notes">{{#note_count}}{{note_count}} notes {{/note_count}}</span><!--<span class="note_link_more" title="7,693 notes">7,693 notes </span>--></div></div></div><div class="post_controls" role="toolbar"><div class="post_controls_inner"><div class="post_control share share_social_button" data-tumblelog-name="{{tumblelog_name}}" data-post-id="{{post)id}}" id="share_social_button_{{post_id}}" title="Share"><div class="popover popover_menu popover_gradient popover_share_social"><div class="popover_inner"><ul class="share_options active"><li class="share_email popover_menu_item"><a href="#">Email</a></li><li class="share_permalink popover_menu_item"><a href="{{post_url}}" target="_blank" class="external" title="Permalink">Permalink </a></li></ul><form action="/svc/email_post" method="post" class="email_form" id="share_email_{{post_id}}" novalidate=""><div class="email_form_wrapper"><div class="input_group"><ul><li><input type="email" class="email_address" name="email_address" maxlength="100" placeholder="Email" title="Email"><input type="hidden" name="post_id" value="{{post_id}}"><input type="hidden" name="tumblelog_name" value="{{tumblelog_name}}"><a href="#" class="cancel" tabindex="-1"></a></li><li><textarea name="message" class="email_message" maxlength="255" placeholder="Message (Optional)" title="Message (Optional)"></textarea></li></ul></div><button type="submit" class="chrome blue" data-label-sending="Sending..." data-label="Send" disabled="">Send</button></div></form><div class="success"><p>Sent!</p></div></div></div></div><a class="post_control reblog" title="Reblog" href="/reblog/{{post_id}}/{{reblog_key}}?redirect_to=%2Fdashboard"><span class="offscreen">Reblog</span></a><div class="post_control like" title="Like"><div class="post_animated_heart post_poof unliked" style="display: none;"><span class="heart_left"></span><span class="heart_right"></span></div></div></div></div></div><div class="notes_outer_container"><div class="notes_container"><ol class="notes"></ol><div class="note more_notes_link_container"><span class="notes_loading" style="display:none;">Loading...</span><a class="more_notes_link" data-next="" rel="nofollow" href="#">Show more notes</a></div></div></div><a class="post_permalink" id="permalink_{{post_id}}" href="{{short_url}}" target="_blank" title="View post"></a></div></div></li>';

function Post(post_json, form_key) {
//	TODO quote sizes
	var post_id = post_json.id;
	var reblog_key = post_json.reblog_key;
	var view = {
		// Universal
			post_type:			determineType(post_json.photoset_layout, post_json.type),
			post_id:			post_id,
			tumblelog_name:		post_json.blog_name,
			reblog_key: 		reblog_key,
			reblogged_name: 	post_json.reblogged_from_name,
			reblogged_url:	 	post_json.reblogged_from_url,
			source_url: 		post_json.source_url,
			source_name: 		post_json.source_title,
			tags: 				post_json.tags,
			full_tag:			function () {
									return '<a class="post_tag" href="/tagged/'
											+ encodeURIComponent(this.replace(/\ /g,'-'))
											+ '">#' + this + '</a>&nbsp;'; 
								},
			note_count: 		tinyFunctions.formatNumber(post_json.note_count),
			short_url:	 		post_json.short_url,
			post_url:			post_json.post_url,
		// Answer
			question:			post_json.question,
			question_asker:		post_json.asking_name,
			question_asker_url:	post_json.asking_url,
			answer:				makeCaption(post_json.answer),
		// Audio
			audio_embed:		post_json.embed,
			audio_download:		makeLink(post_json.audio_url, post_json.embed),
		// Chat
			chat_dialogue:		post_json.dialogue,
			strong_label:		function() {
									if (this.label != '')
										return '<strong>' + this.label + '</strong> ';
									else
										return '';
			},
		// Link
			link_url:			post_json.url,
			link_url_text:		(/http(?:s?):\/\/(?:www\.)?(.+?)(?:$|\/)/.exec(post_json.url)) ? /http(?:s?):\/\/(?:www\.)?(.+?)(?:$|\/)/.exec(post_json.url)[1] : false,
		// Photo
			image:	 			getImage(post_json.photos),
		// Photoset
			photoset:			makePhotoset(post_id, post_json.photoset_layout, post_json.photos),
		// Quote
			quote_text:			post_json.text,
			quote_source:		makeCaption(post_json.source),
		// Text
			text_body:			makeTextBody(post_json.body),
		// Video
			video_player:		getBestPlayer(post_json.player),
			video_url:			post_json.video_url,
		// Audio, photo, photoset, video
			caption: 			makeCaption(post_json.caption, post_json.description),
		// Chat, lext, text
			post_title:			(post_json.title == '') ? false : post_json.title
	};
	var html = $.parseHTML(Mustache.render(POST_TEMPLATE, view));
	
	$(html).find('.like').click(likePost(post_id, reblog_key));
	$(html).find('.reblog').click(fastReblog(post_id, reblog_key));
	$(html).find('.tumblelog_menu_button').click(
			function() {
				$(this).parent().find('#popover_tumblelog_menu').toggle();
			}
	);
	$(html).find('.high_res_link').click(
			function(e) {
				e.preventDefault();
				Lightbox.init(
						[{ 	width: view.image.hd_width,
							height: view.image.hd_height,
							high_res : (view.image.hd_url) ? view.image.hd_url: view.image.url,
							low_res: view.image.url }]);
			}
	);
	$(html).find('.audio_link_generator').click(
			function(e) {
				var xhr = new XMLHttpRequest();
				xhr.open('GET', this.getAttribute('embedurl'));
				xhr.addEventListener('load', updateAudioLink(this));
				xhr.addEventListener('error', updateAudioLink(this));
				xhr.send();
			}
	);
	
	if (view.post_type.photoset) {
		var photoset_photos = new Array();
		for (var row in view.photoset) {
			for (var img in view.photoset[row].row) {
				photoset_photos.push({
					low_res : view.photoset[row].row[img].url,
					high_res : view.photoset[row].row[img].hdurl,
					width : view.photoset[row].row[img].width,
					height : view.photoset[row].row[img].height,
					caption : view.photoset[row].row[img].caption,
				});
			}
		}
		for (var i=1; i<=photoset_photos.length; i++) {
			var link = $(html).find('#photoset_link_' + post_id + '_' + i)[0];
			link.addEventListener('click', function(e) {
				e.preventDefault();
				var i = parseInt($(e.srcElement).parent()[0].id.split(/[_]+/).pop());
				console.log(i);
				Lightbox.init(
						photoset_photos, i);
				});
		}
	}
	
	this.returnHtml = function() {
		return html;
	};
	this.id = function() {
        return post_id;
    };

	function getImage(photos) {	

		if (!photos) return false;
		if (photos.length === 0) return false;

		var alt_sizes = photos[0].alt_sizes;
		var url = '';
		var hd = false;
		var hdurl = '';
		var width = 0;
		var height = 0;
		var alt_width = 0;
		var alt_height = 0;
		var hd_width = 0;
		var hd_height = 0;
		if (alt_sizes[0].width > 500) {
			hd = true;
			hdurl = alt_sizes[0].url;
			hd_width = alt_sizes[0].width;
			hd_height = alt_sizes[0].height;
			url = alt_sizes[1].url;
			width = alt_sizes[1].width;
			height = alt_sizes[1].height;
		} else {
			url = alt_sizes[0].url;
			width = alt_sizes[0].width;
			height = alt_sizes[0].height;
		}
		alt_width = (width > 150) ? 150 : width;
		alt_height = (alt_width == 150) ? Math.round((height * alt_width) / width)
				: height;
		var image = {
			url : url,
			hd : hd,
			hdurl : hdurl,
			width : width,
			height : height,
			alt_width : alt_width,
			alt_height : alt_height,
			hd_width : hd_width,
			hd_height : hd_height
		};
		return image;
	}
	function likePost(post_id, reblog_key) {

		return function(evt) {
			var like_element = 	evt.currentTarget;
			var unlike = $(like_element).hasClass('liked');
			var url = (unlike) ? 'http://www.tumblr.com/svc/unlike' : 'http://www.tumblr.com/svc/like';
			var form_data = 'form_key=' + form_key + '&data%5Bid%5D=' + post_id + '&data%5Bkey%5D=' + reblog_key;

			var xhr = new XMLHttpRequest();
			
			xhr.open('POST', url);
			xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded; charset=UTF-8");
			xhr.addEventListener('load', likeEffect(like_element), false);
			xhr.send(form_data);
		};
	}
	function fastReblog(post_id, reblog_key) {
		return function(evt) {
			if (evt.ctrlKey && evt.altKey) {
				evt.preventDefault();
				var reblog_element = evt.currentTarget;
				var URL = 'http://www.tumblr.com/fast_reblog';
				var form_data = 'reblog_key=' + reblog_key + '&reblog_post_id=' + post_id + '&form_key=' + form_key;
					
				var xhr = new XMLHttpRequest();
				xhr.open('POST', URL);
				xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded; charset=UTF-8");
				xhr.addEventListener('load', reblogEffect(reblog_element), false);
				xhr.send(form_data);
			}
		};
	}
	function likeEffect(like_element) {
	    console.log(like_element);
		return function() {
			$(like_element).toggleClass('liked');
			var heart = $(like_element).find('.post_animated_heart')[0];
			$(heart).toggleClass('unliked');
			$(heart).toggle();
			setTimeout(function () { $(heart).toggle(); }, 600);
		};
	}
	function reblogEffect(reblog_element) {
		return function() {
			$(reblog_element).addClass('animated').addClass('collapse_reblog_button');
	        window.setTimeout($.bind(function() {
	            $(reblog_element).addClass('final_state');
	        }, this), 1000);
		};
	}
	function determineType(photoset_layout, post_type) {
		var type = {
			value :			'',
			class_name :	'',
			data_type :		'',
			text :			false,
			photo :			false,
			photoset :		false,
			quote :			false,
			link :			false,
			chat :			false,
			audio :			false,
			video :			false,
			answer :		false
		};
		if (photoset_layout) {
			type.value = 'photoset';
			type.class_name = 'is_photoset';
			type.data_type = 'photoset';
			type.photoset = true;
		} else {
			type.value = post_type;
			type[post_type] = true;
			switch (post_type) {
				case 'text':
					type.class_name = 'is_regular';
					type.data_type = 'regular';break;
				case 'photo':
					type.class_name = 'is_photo';
					type.data_type = 'photo';break;
				case 'quote':
					type.class_name = 'is_quote';
					type.data_type = 'quote';break;
				case 'link':
					type.class_name = 'is_link';
					type.data_type = 'link';break;
				case 'chat':
					type.class_name = 'is_conversation';
					type.data_type = 'conversation';break;
				case 'audio':
					type.class_name = 'is_audio';
					type.data_type = 'audio';break;
				case 'video':
					type.class_name = 'is_video';
					type.data_type = 'video';break;
				case 'answer':
					type.class_name = 'is_note';
					type.data_type = 'note';break;
				default:
					type.class_name = 'is_fail';
		    }
		}
		return type;
	}
	function makePhotoset(id, layout, photos) {
		
		if (!layout) return false;
		
		var number_of_rows = layout.length;
		var photoset = new Array(number_of_rows);
		var number_of_photo = 0;
		for (var i=0; i<number_of_rows; i++) {
			var photos_in_row = parseInt(layout.charAt(i));
			var row = new Array(photos_in_row);
			var minheight = 0;
			for (var j=0; j<photos_in_row; j++) {
				row[j] = getImagePhotoset(photos[number_of_photo], photos_in_row, number_of_photo);
				if (minheight > row[j].alt_height || minheight == 0) {
					minheight = row[j].alt_height;
				}
				number_of_photo++;
			}
			for (var j=0; j<photos_in_row; j++) {
				if (row[j].alt_height > minheight) {
					row[j].margin = 'margin-top:-' + Math.round((row[j].alt_height - minheight) / 2) + 'px; ';
				}
			}
			photoset[i] = { row : row, minheight : minheight, photos_in_row : photos_in_row };
		}

		return photoset;
	}
	
	function getImagePhotoset(photo, photos_in_row, number_of_photo) {
		var caption = photo.caption;
		var url = '';
		var hdurl = photo.alt_sizes[0].url;
		var width = 0;
		var height = 0;
		for (var size in photo.alt_sizes) {
			if (photos_in_row>1) {
				if (photo.alt_sizes[size].width<=250) {
					url = photo.alt_sizes[size].url;
					width = photo.alt_sizes[size].width;
					height = photo.alt_sizes[size].height;
					break;
				}
			} else {
				url = photo.alt_sizes[size].url;
				width = photo.alt_sizes[size].width;
				height = photo.alt_sizes[size].height;
				break;
			}
		}
		var alt_width = (500 - (photos_in_row-1)*10)/photos_in_row;
		var alt_height = Math.round((alt_width*height)/width);
		var image = {
			caption : caption,
			url : url,
			hdurl : hdurl,
			width : width,
			height : height,
			alt_width : alt_width,
			alt_height : alt_height,
			number : number_of_photo+1,
			margin : ''
		};
		return image;
	}
	function makeCaption(caption, link_description) {
		
		if (!caption && !link_description) return false;
		if (caption == '' && link_description == '') return false;
		if (!caption) caption = link_description;
		
		var patternHeadline = /<h\d.*?>(.*?)<\/h\d>/ig;
		caption = caption.replace(patternHeadline, '$1');
		
		var patternSpan = /<span.*?>(.*?)<\/span>/ig;
		caption = caption.replace(patternSpan, '$1');
		
		var patternLinkPic = /<a [^>]*?>(.*?<img.*?)<\/a>/ig;
		caption = caption.replace(patternLinkPic, '$1');
		
		var internalPicAttr = '<img onclick="toggle_inline_photo_thumbnails(this); return false;" style="cursor:pointer;" class="toggle_inline_image inline_image constrained_image" ';
		var patternInternal = /<img ([^>]*src="http:\/\/media.tumblr.com\/.+?\/>)/ig;
		caption = caption.replace(patternInternal, '<p>' + internalPicAttr + '$1' + '</p>');
		
		var extHyperlink = '<img external_src="$1" src="http://assets.tumblr.com/images/inline_photo.png?2" loader="http://assets.tumblr.com/images/inline_photo_loading.gif" class="inline_external_image constrained_image" width="44" height="49" onclick="toggle_inline_photo_thumbnails(this); return false;">';
		var patternExternal = /<img (?!o)[^>]*src="(.*?)".*?\/>/ig;
		caption = caption.replace(patternExternal, '<p>' + extHyperlink + '</p>');
		
		
		
		return caption;
	}
	function makeTextBody(body, post_url) {
		
		if (!body) return false;

		var splitbody = body.split('<!-- more -->', 2);
		if (splitbody.length > 1) {
			var read_more = '<p class="read_more_container"><a href="' + post_url + '" class="read_more">Read More</a> &#8594;</p>';
			return makeCaption(splitbody[0] + read_more);
		}
		return makeCaption(body);
	}
	function getBestPlayer(players) {
		
		if (!players) return false;
		if (players.length === 0) return false;
		
		var i = players.length - 1;
		
		return players[i].embed_code;
	}
	function makeLink(audiourl, embed) {
		
		if (!audiourl) return false;
		if (!embed) return false;
		
		var result = /src="(.*?)"/.exec(embed);
		
		if (!result) return false;
		if (result.length < 2) return false;
		
		var url = result[1];
		return '<a class="audio_link_generator" audiourl="' + audiourl + '" embedurl="' + url + '" style="font-size: 10px;" target="_blank">Generate download link</a>';
	}
	function updateAudioLink(link) {
			return function(e) {
				if (e.type == 'load') {
					var result = /data-post-key="(.*?)"/.exec(e.srcElement.responseText);
					if (result && result.length >= 2) {
						var key = result[1].replace(/&amp;/g, '&');
						link.setAttribute('href', link.getAttribute('audiourl') + '?play_key=' + key);
						link.innerHTML = 'Right-click-save-link-as me';
					}
				} else {
					link.innerHTML = 'Failed :(';
				}
			};
	}
}
/* Below ONLY LightBox rip off */
var Lightbox = window.Lightbox = (function() {
    var f = false;
    var h = false;
    var n = false;
    var d = [];
    var i = false;
    var g = false;
    var o = false;
    var e = false;
    var c = { left: false, center: false, right: false };
    function init(r, u) {
        if (document.getElementById("tumblr_lightbox")) {
            return
        }
        if (!u) {
            u = 1;
        }
        d = r;
        window.focus();
        if (!f) {
            if (window.onkeydown) {
                h = window.onkeydown;
            }
            window.onkeydown = function(B) {
                if (document.getElementById("tumblr_lightbox")) {
                    if (!B) {
                        B = window.event;
                    }
                    var A = B.charCode ? B.charCode : B.keyCode;
                    if (!B.shiftKey && !B.ctrlKey && !B.altKey && !B.metaKey) {
                        if (A == 37) {
                            if (g > 1) {
                                k(g - 1);
                            }
                        } else {
                            if (A == 39) {
                                if (g < d.length) {
                                    k(g + 1);
                                }
                            } else {
                                if (A == 27 || A == 32 || A == 74 || A == 75) {
                                    l();
                                }
                            }
                        }
                    } else {
                        if ((B.ctrlKey || B.metaKey) && A == 87) {
                            l();
                            return false;
                        }
                    }
                }
                if (h) {
                    h();
                }
            };
            if (window.onresize) {
                n = window.onresize;
            }
            window.onresize = function() {
                if (document.getElementById("vignette")) {
                    document.getElementById("vignette").style.display = "none";
                    if (e) {
                        clearTimeout(e);
                    }
                    e = setTimeout(function() {
                        document.getElementById("vignette").style.display = "inline-block";
                    }, 100);
                }
                j();
                if (n) {
                    n();
                }
            };
            if (navigator && navigator.userAgent.search("iPad") != -1) {
                document.addEventListener("touchmove", function() {
                    l();
                }, false);
            }
            f = true;
        }
        document.body.style.overflow = "hidden";
        var p = document.createElement("div");
        p.setAttribute("id", "tumblr_lightbox");
        if (navigator && navigator.userAgent.search("iPad") != -1) {
            p.style.position = "absolute";
            p.style.top = document.body.scrollTop + "px";
            p.style.height = window.innerHeight + "px";
        } else {
            p.style.position = "fixed";
            p.style.top = "0px";
            p.style.bottom = "0px";
        }
        p.style.left = "0px";
        p.style.right = "0px";
        p.style.zIndex = 4294967294;
        p.style.overflow = "hidden";
        p.style.backgroundColor = (navigator && navigator.userAgent.indexOf("MSIE") != -1) ? "#222" : "rgba(17,17,17,0.92)";
        p.onclick = function() {
            if (i) {
                i = false;
            } else {
                l();
            }
        };
        if (!(navigator && navigator.userAgent.search("iPad") != -1) && !(navigator && navigator.userAgent.search("MSIE") != -1)) {
            var x = document.createElement("img");
            x.setAttribute("id", "vignette");
            x.setAttribute("src", "http://assets.tumblr.com/images/full_page_vignette.png");
            x.style.position = "absolute";
            x.style.width = "100%";
            x.style.height = "100%";
            x.style.left = "0px";
            x.style.top = "0px";
            p.appendChild(x);
            var w = document.createElement("div");
            w.style.position = "absolute";
            w.style.width = "100%";
            w.style.height = "100%";
            w.style.left = "0px";
            w.style.top = "0px";
            p.appendChild(w);
        }
        var s = document.createElement("div");
        s.style.position = "absolute";
        s.style.left = "50%";
        s.style.top = "50%";
        if (!document.getElementById("tumblr_form_key")) {
            s.style.width = "100%";
        }
        p.appendChild(s);
        var q = ["left", "center", "right"];
        while (stage_name = q.pop()) {
            var v = document.createElement("a");
            v.setAttribute("id", "tumblr_lightbox_" + stage_name + "_link");
            v.setAttribute("href", "#");
            if (d.length < 2) {
                v.style.cursor = "default";
            }
            s.appendChild(v);
            var t = document.createElement("img");
            t.setAttribute("id", "tumblr_lightbox_" + stage_name + "_image");
            t.setAttribute("src", "http://assets.tumblr.com/images/x.gif");
            t.style.mozBorderRadius = "3px";
            t.style.webkitBorderRadius = "3px";
            t.style.borderRadius = "3px";
            t.style.moxBoxShadow = "0 4px 30px rgba(0,0,0,1)";
            t.style.webkitBoxShadow = "0 4px 30px rgba(0,0,0,1)";
            t.style.boxShadow = "0 4px 30px rgba(0,0,0,1)";
            t.style.borderWidth = "0px";
            t.style.position = "absolute";
            if (stage_name == "center") {
                t.style.zIndex = 4294967295;
            }
            v.appendChild(t);
        }
        var z = document.createElement("div");
        z.setAttribute("id", "tumblr_lightbox_caption");
        z.style.position = "absolute";
        z.style.textAlign = "center";
        z.style.font = "bold 17px 'HelveticaNeue','Helvetica','Arial',sans-serif";
        z.style.color = "#fff";
        z.style.paddingTop = "20px";
        z.style.textShadow = "0 4px 30px rgba(0,0,0,1)";
        z.style.display = "inline-block";
        z.style.textRendering = "optimizeLegibility";
        s.appendChild(z);
        document.body.appendChild(p);
        k(u);
        j();
    }
    function l() {
        document.body.style.overflow = "";
        document.getElementById("tumblr_lightbox").style.display = "none";
        document.body.removeChild(document.getElementById("tumblr_lightbox"));
    }
    function k(q) {
        g = q;
        o = Math.round(Math.random() * 1000000000000);
        document.getElementById("tumblr_lightbox_left_link").onclick = function() {
            i = true;
            k(q - 1);
            return false;
        };
        if (d.length == 1) {
            document.getElementById("tumblr_lightbox_center_link").onclick = function() {
                return false;
            };
        } else {
            if (q < d.length) {
                document.getElementById("tumblr_lightbox_center_link").onclick = function() {
                    i = true;
                    k(q + 1);
                    return false;
                };
            } else {
                document.getElementById("tumblr_lightbox_center_link").onclick = function() {
                    i = true;
                    k(1);
                    return false;
                };
            }
        }
        document.getElementById("tumblr_lightbox_right_link").onclick = document.getElementById("tumblr_lightbox_center_link").onclick;
        c.left = false;
        c.center = false;
        c.right = false;
        b("center", q - 1);
        if (q > 1) {
            b("left", q - 2);
        }
        if (q < d.length) {
            b("right", q);
        }
        if (q + 1 < d.length) {
            var p = new Image();
            p.src = d[q + 1].low_res;
        }
    }
    function b(q, s) {
        var p = new Image();
        var r = false;
        p.className = o;
        p.onload = function() {
            if (this.className == o) {
                this.className = "high-res";
                c[q] = this;
                j();
            }
        };
        console.log(d,s,q);
        p.src = d[s].high_res;
        if (!p.complete) {
            r = new Image();
            r.className = o;
            r.onload = function() {
                if (this.className == o && (!c[q] || c[q].className == "placeholder")) {
                    this.className = "low-res";
                    c[q] = this;
                    j();
                }
            };
            r.src = d[s].low_res;
            if (d[s].width && d[s].height) {
                if (r) {
                    r.style.maxWidth = d[s].width + "px";
                    r.style.maxHeight = d[s].height + "px";
                }
                p.style.maxWidth = d[s].width + "px";
                p.style.maxHeight = d[s].height + "px";
            }
            if (!r.complete && (d[s].width && d[s].height)) {
                c[q] = new Image(d[s].width, d[s].height);
                c[q].style.maxWidth = d[s].width + "px";
                c[q].style.maxHeight = d[s].height + "px";
                c[q].src = "http://assets.tumblr.com/images/x.gif";
                c[q].className = "placeholder";
            }
        }
    }
    function j() {
        var u = ["right", "left", "center"];
        while (stage_name = u.pop()) {
            var r = document.getElementById("tumblr_lightbox_" + stage_name + "_image");
            if (!r) {
                continue;
            }
            var s = c[stage_name];
            if (!s) {
                r.style.display = "none";
                continue;
            } else {
                r.style.display = "inline-block";
            }
            var q = s.style.maxWidth ? parseInt(s.style.maxWidth, 10) : s.width;
            var p = s.style.maxHeight ? parseInt(s.style.maxHeight, 10) : s.height;
            if (tumblr_window_dimensions().width / tumblr_window_dimensions().height < q / p) {
                var t = (d.length == 1) ? 0.85 : 0.75;
                if (tumblr_window_dimensions().width * t > q && (s.className == "high-res" || s.style.maxWidth)) {
                    r.style.width = q + "px";
                    r.style.height = p + "px";
                } else {
                    r.style.height = (p * ((tumblr_window_dimensions().width * t) / q)) + "px";
                    r.style.width = (tumblr_window_dimensions().width * t) + "px";
                }
            } else {
                if (tumblr_window_dimensions().height * 0.85 > p && (s.className == "high-res" || s.style.maxHeight)) {
                    r.style.width = q + "px";
                    r.style.height = p + "px";
                } else {
                    r.style.width = (q * ((tumblr_window_dimensions().height * 0.85) / p)) + "px";
                    r.style.height = (tumblr_window_dimensions().height * 0.85) + "px";
                }
            }
            if (stage_name == "center") {
                r.style.left = (0 - parseInt(r.style.width, 10) / 2) + "px";
                r.style.top = (0 - parseInt(r.style.height, 10) / 2) + "px";
            } else {
                r.style[stage_name] = (0 - (parseInt(r.style.width, 10) + tumblr_window_dimensions().width * 0.42)) + "px";
                r.style.top = (0 - parseInt(r.style.height, 10) / 2) + "px";
            }
            r.src = s.src;
            r.style.backgroundColor = (s.className == "placeholder") ? ((navigator && navigator.userAgent.indexOf("MSIE") != -1) ? "#444" : "rgba(255,255,255,0.05)") : "transparent";
            if (stage_name == "center" && d[g - 1].caption) {
                document.getElementById("tumblr_lightbox_caption").innerHTML = d[g - 1].caption;
                document.getElementById("tumblr_lightbox_caption").style.width = (tumblr_window_dimensions().width * 0.7) + "px";
                document.getElementById("tumblr_lightbox_caption").style.top = (parseInt(r.style.height, 10) * 0.5) + "px";
                document.getElementById("tumblr_lightbox_caption").style.left = (0 - tumblr_window_dimensions().width * 0.35) + "px";
                document.getElementById("tumblr_lightbox_caption").style.display = "block";
            } else {
                if (stage_name == "center") {
                    document.getElementById("tumblr_lightbox_caption").style.display = "none";
                }
            }
        }
    }
    function a() {
        return !!document.getElementById("tumblr_lightbox");
    }
    function tumblr_window_dimensions() {
        if (window.innerWidth !== undefined) {
            return {
                width: window.innerWidth,
                height: window.innerHeight
            };
        } else {
            if (document.documentElement) {
                return {
                    width: document.documentElement.clientWidth,
                    height: document.documentElement.clientHeight
                };
            } else {
                return {
                    width: document.body.clientWidth,
                    height: document.body.clientHeight
                };
            }
        }
    }
    return { init: init,isOpen: a };
})();
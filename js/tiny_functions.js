var tinyFunctions = {
    escapeHtml: function (string) {
        var entityMap = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#39;',
            '/': '&#x2F;'
        };
        return String(string).replace(/[&<>"'\/]/g,
            function (s) {
                return entityMap[s];
            });
    },
    checkurl: function (url, filter) {
        for (var f in filter) {
            var filterRegex;
            filterRegex = filter[f].replace(/\x2a/g, '(.*?)');
            var re = new RegExp(filterRegex);
            if (url.match(re)) return true;
        }
        return false;
    },
    is_empty: function (obj) {
        if (obj.length && obj.length > 0) return false;
        if (obj.length && obj.length === 0) return true;
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)) return false;
        }
        return true;
    },
    compare: function (a, b) {
        if (a.timestamp < b.timestamp) return 1;
        if (a.timestamp > b.timestamp) return -1;
        return 0;
    },
    formatNumber : function (number) {
        if (typeof number != 'number') return number; //shouldn't happen but just in case
        if (number === 0) return 0;
        var numberString = number.toString();
        var formatted = '';
        var mod = numberString.length % 3;
        var i = numberString.length - mod;
        formatted += numberString.substr(0, mod);
        if (i > 0) {
            if (mod > 0) formatted += ',';
            while (i > 0) {
                formatted += numberString.substr(numberString.length-i, 3);
                i += -3;
                if (i>0) {
                    formatted += ',';
                }
            }
        }
        return formatted;
    }
};
